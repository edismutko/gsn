// The utils is a function library for general purposes
var Utils = require('../utils/utils.js');

// Validate the req parameter for correct body information
// needed for updating the user account
// User can only update email
// the rest of the data is not changeable
function updateUserValidation(req, res)
{
	if (req.body)
	{
		if ((typeof(req.body.email) == "undefined") || 
		    (Utils.IsString(req.body.email)))
		{			
			return true;
		}
		else
		{
			res.json(Utils.HandleError(null,"The request body must contain [email] string!"));
		}
	}
	else
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
	}
	return false;
}

// Validate the req parameter for correct body information
// needed for updating the user account balance
function updateAccountBalanceValidation(req, res)
{
	var message = "";
	if (req.body)
	{
		if (!((typeof(req.body.paypal_token) == "undefined") || 
		     (Utils.IsString(req.body.paypal_token))))
		{			
			message += "The request body must contain [paypal_token] string!\n";
		}
		if ((typeof(req.body.account_balance) == "undefined") || 
		    (Utils.IsNumeric(req.body.account_balance)))
		{			
			message += "The request body must contain [account_balance] numeric!\n";
		}
		if (!(message))
		{
			res.json(Utils.HandleError(null,message));
		}
		else
		{
			true;
		}
	}
	else
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
	}
	return false;
}

module.exports = {
	UpdateUserValidation: updateUserValidation,
	UpdateAccountBalanceValidation: updateAccountBalanceValidation
}

