// The utils is a function library for general purposes 
var Utils = require("../utils/utils.js");

// Validate the req parameter for correct body information 
// needed for inserting a new gamble
function insertGambleValidation(req, res, user_id, bet_id)
{
	var message = "";
	if (!req.body)
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
		return false;
	}
	
	if (!Utils.IsString(req.body.user_id))
	{	
		message += "The request body must contain [user_id] string!\n";
	}
	else if (req.body.user_id != user_id)
	{
		res.json(Utils.HandleError(null,"The gamble [user_id] is out of context!"));
		return false;
	}	
	if (!Utils.IsString(req.body.username))
	{
		message += "The request body must contain [username] string!\n";
	}	
	if (!Utils.IsString(req.body.bet_id))
	{	
		message += "The request body must contain [bet_id] string!\n";
	}
	else if (req.body.bet_id != bet_id)
	{
		res.json(Utils.HandleError(null,"The gamble [bet_id] is out of context!"));
		return false;
	}
	if (!Utils.IsString(req.body.bet_title))
	{
		message += "The request body must contain [bet_title] string!\n";
	}
	if (!req.body.bet_option)
	{
		message += "The request body must contain [bet_option] json!\n";
	}
	if (!Utils.IsDate(new Date(req.body.bet_start_date)))
	{
		message += "The request body must contain [bet_start_date] date!\n";
	}
	if (!Utils.IsDate(new Date(req.body.bet_end_date)))
	{
		message += "The request body must contain [bet_end_date] date!\n";
	}
	if (message) 
	{
		res.json(Utils.HandleError(null, message));
		return false;
	}
	else 
	{
		return true;
	}
}

// Validate the req parameter for correct body information 
// needed for updating a existing gamble
// Only the bet_option is available for update
function updateGambleValidation(req, res)
{
	if (!req.body)
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
		return false;
	}
	if (!req.body.bet_option)
	{
		res.json(Utils.HandleError(null,"The request body must contain [bet_option] json!"));
		return false;
	}
	return true;
}

module.exports = {
	InsertGambleValidation: insertGambleValidation,
	UpdateGambleValidation: updateGambleValidation
}