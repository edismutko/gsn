// The utils is a function library for general purposes
var Utils = require("../utils/utils.js");

// Validate the req parameter for correct body information 
// needed for login the user, the login logic is paypal dependent
// so the paypal token acts as the login token, 
// no need for user password on login
function loginUserValidation(req, res)
{
	if (req.body)
	{
		if (Utils.IsString(req.body.paypal_token)) return true;
		else res.json(Utils.HandleError(null,"The request body must contain [paypal_token] string!"));
	}
	else
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
	}
	return false;
}

// Validate the req parameter for correct body information 
// needed for signup the user, that includes all user information
// based on paypal
function signUpUserValidation(req, res)
{	
	var message;
	if (!req.body)
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
		return false;
	}
	
	if (!Utils.IsString(req.body.paypal_token))
	{
		message = "The request body must contain [paypal_token] string!\n";
	}
	if (!Utils.IsString(req.body.username))
	{
		message = "The request body must contain [username] string!\n";
	}
	if (!Utils.IsString(req.body.first_name))
	{
		message = "The request body must contain [first_name] string!\n";
	}
	if (!Utils.IsString(req.body.last_name))
	{
		message = "The request body must contain [last_name] string!\n";
	}
	if (!Utils.IsString(req.body.email))
	{
		message = "The request body must contain [email] string!\n";
	}
	
	if (message) 
	{
		res.json(Utils.HandleError(null, message));
		return false;
	}
	else 
	{
		return true;
	}
}

module.exports = {
	LoginUserValidation: loginUserValidation,
	SignUpUserValidation: signUpUserValidation
}
