// The utils is a function library for general purposes 
var Utils = require("../utils/utils.js");

// Validate the req parameter for correct body information 
// needed for inserting a new bet
function insertBetValidation(req, res, is_private)
{
	var message = "";
	if (!req.body)
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
		return false;
	}
	
	if (!Utils.IsString(req.body.title))
	{
		message += "The request body must contain [title] string!\n";
	}
	if (!Utils.IsString(req.body.description))
	{
		message += "The request body must contain [description] string!\n";
	}
	if (!Utils.IsArray(req.body.options))
	{
		message += "The request body must contain [options] array[json]!\n";
	}
	if (!Utils.IsDate(new Date(req.body.start_date)))
	{
		message += "The request body must contain [start_date] date!\n";
	}
	if (!Utils.IsDate(new Date(req.body.end_date)))
	{
		message += "The request body must contain [end_date] date!\n";
	}
	if (!Utils.IsNumeric(req.body.amount))
	{
		message += "The request body must contain [amount] numeric!\n";
	}
	if (is_private)
	{
		if (!req.body.admin)
		{
			message += "The request body must contain [admin] json!\n";
		}
		if (!Utils.IsArray(req.body.users))
		{
			message += "The request body must contain [users] array[json]!\n";
		}
	}
	
	if (message) 
	{
		res.json(Utils.HandleError(null, message));
		return false;
	}
	else 
	{
		return true;
	}
}

// Validate the req prameter for correct body information 
// needed for updating a bet
// the only  fields available for update are 
// description, users
function updateBetValidation(req, res, is_private)
{
	var message = "";
	if (!req.body)
	{
		res.json(Utils.HandleError(null,"The request body must be in json format!"));
		return false;
	}

	if ((typeof(req.body.description) != "undefined") && 
		(!Utils.IsString(req.body.description)))
	{
		message += "The request body must contain [description] string!\n";
	}
	if (is_private)
	{
		if ((typeof(req.body.users) != "undefined") &&
		    (!Utils.IsArray(req.body.users)))
		{
			message += "The request body must contain [users] array[json]!\n";
		}
	}
	
	if (message) 
	{
		res.json(Utils.HandleError(null, message));
		return false;
	}
	else 
	{
		return true;
	}
}

module.exports = {
	InsertBetValidation: insertBetValidation,
	UpdateBetValidation: updateBetValidation
}