var mongoose  = require('mongoose');
var Schema  = mongoose.Schema;

var account_activity = new Schema({
	bet_id : {type:String, index:true},
	bet_title : String,
	user_id : {type:String, index:true},
	username : String,
	description : String,
	amount : {type:Number, default:0},
	record_date : { type:Date, default:Date.now }
});
account_activity.index({bet_id:true,user_id:true});

module.exports = mongoose.model('account_activity', account_activity);