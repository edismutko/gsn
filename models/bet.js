var mongoose  = require('mongoose');
var Schema = mongoose.Schema;

var bet = new Schema({
	title : String,
	description : String,
	options : [{ option: String }],
	result : {type:{ option: String }, default:null},
	amount : {type:Number, default:0},
	start_date : Date,
	end_date : Date,
	record_date : { type: Date, default: Date.now },
	is_private : { type: Boolean, default: false },
	admin : {type:{ user_id : String, username : String}, sparse: true}, 
	users : [{ user_id : {type:String, index:true}, username : String}]
});

module.exports = mongoose.model('bet', bet);
