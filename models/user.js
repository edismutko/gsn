var mongoose  = require('mongoose');
var Schema  = mongoose.Schema;

var user = new Schema({
	username : {type:String, unique:true},
	email : String,
	first_name : String,
	last_name : String,
	paypal_token : {type:String, unique:true},
	account_balance : {type:Number, default:0},
	record_date : {type:Date, default:Date()}
});

module.exports = mongoose.model('user', user);