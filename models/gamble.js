var mongoose  = require('mongoose');
var Schema  = mongoose.Schema;

var gamble = new Schema({
	user_id : { type:String, index:true },
	username : String,
	bet_id : { type:String, index:true },
	bet_title : String,
	bet_option : { option: String },
	bet_start_date : Date,
	bet_end_date : Date,
	is_win : Boolean,
	record_date : { type:Date, default: Date.now }
});

module.exports = mongoose.model('gamble', gamble);