// Gamble controller for action control
var Gamble = require("../controllers/gamble.js");

// Setup the router with the relevant routings for the gamble feature
// get => read
// post => insert
// put => update 
// delete => delete
module.exports = {
	SetupRouter: function(router){
		router.get("/users/:user_id/bets/:bet_id/gambles/",function(req,res){
		  Gamble.GetGamble(req, res, req.params.user_id, req.params.bet_id);
		})
		.post("/users/:user_id/bets/:bet_id/gambles/",function(req,res){
		  Gamble.InsertGamble(req, res, req.params.user_id, req.params.bet_id);
		})
		.put("/users/:user_id/bets/:bet_id/gambles/:gamble_id",function(req,res){
		  Gamble.UpdateGamble(req, res, req.params.user_id, req.params.bet_id, req.params.gamble_id);
		})
		.delete("/users/:user_id/bets/:bet_id/gambles/:gamble_id",function(req,res){
		  Gamble.DeleteGamble(req, res, req.params.user_id, req.params.bet_id, req.params.gamble_id);
		});
	}
}