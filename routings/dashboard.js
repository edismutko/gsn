// Dashboard controller for action control
var Dashboard = require("../controllers/dashboard.js");

// Setup the router with the relevant routings for the dashboard feature
// get => read
// post => insert
module.exports = {
	SetupRouter: function(router){
		router.get("/users/:user_id/dashboard/available_bets/",function(req,res){
		  Dashboard.GetAvilablePublicBets(req, res, req.params.user_id);
		})
		.get("/users/:user_id/dashboard/private_bets/",function(req,res){
		  Dashboard.GetUserPrivateBets(req, res, req.params.user_id);
		})
		.get("/users/:user_id/dashboard/active_bets",function(req,res){
		  Dashboard.GetUserActiveBets(req, res, req.params.user_id);
		})
		.get("/users/:user_id/dashboard/bet_winnings/",function(req,res){
		  Dashboard.GetUserGamblesWinnings(req, res, req.params.user_id);
		})
		.get("/users/:user_id/dashboard/bet_losses/",function(req,res){
		  Dashboard.GetUserGamblesLosses(req, res, req.params.user_id);
		})
		.get("/users/:user_id/dashboard/gambles/",function(req,res){
		  Dashboard.GetUserGambles(req, res, req.params.user_id);
		})
		.post("/users/:user_id/dashboard/bets/:bet_id/gamble/",function(req,res){
		  Dashboard.InsertUserGamble(req, res, req.params.user_id, req.params.bet_id);
		});
	}
}