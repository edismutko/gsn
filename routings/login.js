// Login controller for action control
var Login = require("../controllers/login.js");

// Setup the router with the relevant routings for the login feature
// get => read
// post => insert
// put => update 
// delete => delete
module.exports = {
	SetupRouter: function(router){
		router.post("/users/login", function(req,res){
		  Login.LoginUser(req, res);
		})
		router.post("/users/signup", function(req,res){
		  Login.SignUpUser(req, res);
		});
	}
}
