var Gen = require("../controllers/test_generator.js");

module.exports = {
	SetupRouter: function(router){
		router.post("/testgen/users", function(req,res){
		  Gen.GenerateUsers(req,res);
		})
		.post("/testgen/bets/public/old", function(req,res){
		  Gen.GenerateOldPublicBets(req,res,10);
		})
		.post("/testgen/bets/public/available", function(req,res){
		  Gen.GenerateAvailablePublicBets(req,res,20);
		})
		.post("/testgen/bets/public/active", function(req,res){
		  Gen.GenerateActivePublicBets(req,res,10);
		})
		.post("/testgen/gambles", function(req,res){
		  Gen.GenerateUserGambles(req,res);
		})
	}
}
