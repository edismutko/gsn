// Account controller for action control
var Account = require("../controllers/account.js");

// Setup the router with the relevant routings for the account feature
module.exports = {
	SetupRouter: function(router){
		router.get("/users/:user_id/account",function(req,res){
		  Account.GetUser(req, res, req.params.user_id);
		})
		.put("/users/:user_id/account",function(req,res){
		  Account.UpdateUser(req, res, req.params.user_id);
		})
		.get("/users/:user_id/account/activities",function(req,res){
		  Account.GetUserAccountActivities(req, res, req.params.user_id);
		})
		.put("/users/:user_id/account/balance",function(req,res){
		  Account.UpdateUser(req, res, req.params.user_id);
		});
	}
}