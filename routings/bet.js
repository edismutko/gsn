// Bet controller for action control
var Bet = require("../controllers/bet.js");

// Setup the router with the relevant routings for the bet feature
// get => read
// post => insert
// put => update 
module.exports = {
	SetupRouter: function(router){
		router.get("/users/:user_id/bets/:bet_id",function(req,res){
		  Bet.GetBet(req, res, req.params.bet_id);
		})
		.post("/users/:user_id/bets/",function(req,res){
		  Bet.InsertPrivateBet(req, res);
		})
		.put("/users/:user_id/bets/:bet_id",function(req,res){
		  Bet.UpdateBet(req, res, req.params.bet_id);
		});
	}
}