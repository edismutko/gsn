// The utils is a function library for general purposes 
var Utils = require('../utils/utils.js');
// The user model (mongoose schema)
var User = require('../models/user');
// The account activity model  (mongoose schema)
var AccountActivity = require('../models/account_activity'); 
// The account validator contains request validation functions
var Validator = require('../validators/account_validator.js');

// Returns the user account details
function getUser(req, res, user_id)
{
	User.findById(user_id, function (err, user) 
	{
		if ((err) || (!user)) res.json(Utils.HandleError(err, "Could not retreive user!"));
		else res.json(user);
	});
}

// Update the user account details & returns the updated user model.
// Most fields comes from the paypal account on sign up, 
// this is why only the email field is updatable
function updateUser(req, res, user_id)
{
	if (!Validator.UpdateUserValidation(req, res)) return;
	
	User.findById(user_id, function (err, user) 
	{
		if ((err) || (!user))
		{
			res.json(Utils.HandleError(err, "Could not find user!"))
		}
		else
		{
			if (typeof(req.body.email) != "undefined") 
				user.email = req.body.email;
			user.save(function(doc)
			{
				res.json(user);
			},
			function(err)
			{
				res.json(Utils.HandleError(err, "Could not update user!"));
			});
		}
	});
}

// Returns all user's account activities
function getUserAccountActivities(req, res, user_id)
{
	AccountActivity.find({user_id : user_id},
	function(err, activities)
	{	
		if (err) res.json(Utils.handleError(err, "Could not retreive user's account activities!"));
		else res.json(activities);
	});	
}

// Update user's account balance & returns the updated user model.
// This function also adds a new account activity [Set new account balance]
// for the user's account activities.
function updateAccountBalance(req, res, user_id)
{
	if (!Validator.UpdateAccountBalanceValidation(req, res)) return;
	
	User.findOne({_id:user_id,paypal_token:paypal_token}, 
	function (err, user) 
	{
		if ((err) || (!user)) res.json(Utils.HandleError(err, "Could not find account!"));
		else
		{
			if (typeof(req.body.account_balance) != "undefined") 
				user.account_balance = req.body.account_balance;
			user.save(function(doc)
			{	
				var activity = new AccountActivity();
				activity.user_id = user._id;
				activity.username = user.username;
				activity.description = "Set new account balance!";
				activity.amount = user.account_balance;
				var promise = activity.save();
				promise.then(
				function(newActivity)
				{
					res.json(user);
				},
				function(err)
				{
					res.json(Utils.HandleError(err, "Could not insert account activity!"));
				});
			},
			function(err)
			{
				res.json(Utils.HandleError(err, "Could not update account balance!"));
			});
		}
	});
}

module.exports = {
	GetUser: getUser,
	UpdateUser: updateUser,
	GetUserAccountActivities:getUserAccountActivities,
	UpdateAccountBalance: updateAccountBalance
}

