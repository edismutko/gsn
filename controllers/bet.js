// The utils is a function library for general purposes 
var Utils = require('../utils/utils.js');
// The bet model (mongoose schema)
var Bet = require('../models/bet');
// The bet validator contains request validation functions
var Validator = require('../validators/bet_validator.js');

// Returns the bet according to the given bet_id 
function getBet(req, res, bet_id)
{
	Bet.findById(bet_id, 
		function (err, bet) 
		{
			if ((err) || (!bet)) res.json(Utils.handleError(err, "Could not find bet!"));
			else res.json(bet);
		});
}

// Update the bet corrisponding the given bet_id
// and return the updated bet back to the client
// The fields that can be update are:
// description, users
// rest of the fields can't  be updated and should 
// be only decided on insert.
function updateBet(req, res, bet_id)
{
	if (!Validator.UpdateBetValidation(req,res)) return;	
	Bet.findById(bet_id, function (err, bet) 
	{
		if ((err) || (!bet)) 
		{
			res.json(Utils.HandleError(err, "Could not find bet!"))
		}
		else
		{
			if (typeof(req.body.description) != "undefined")
				bet.description = req.body.description;
			if (bet.is_private)
			{
				if (typeof(req.body.users) != "undefined")
					bet.users = req.body.users;
			}
			bet.save(function() 
			{
				res.json(bet);
			},
			function(err)
			{
				res.json(Utils.HandleError(err, "Could not update bet!"));
			});
		}
	});
}

// Insert a new private bet
function insertPrivateBet(req, res)
{	
	insertBet(req, res, true);
}

// Insert a new public bet
function insertPublicBet(req, res)
{	
	insertBet(req, res, false);
}

// helper function for inserting
function insertBet(req, res, is_private)
{	
	if (!Validator.InsertBetValidation(req,res, is_private)) return;
	var bet = new Bet();
	bet.title = req.body.title;
	bet.description = req.body.description;
	bet.options = req.body.options;
	bet.amount = req.body.amount;
	bet.start_date = req.body.start_date;
	bet.end_date = req.body.end_date;
	if (is_private)
	{
		bet.is_private = true;
		bet.admin = req.body.admin;
		bet.users = req.body.users;
	}
	var promise = bet.save();
	promise.then(function(doc)
	{
		res.json(doc);
	},
	function(err)
	{
		res.json(Utils.HandleError(err,"Could not insert bet!"));
	});
}

module.exports = {
	GetBet: getBet,
	UpdateBet: updateBet,
	InsertPrivateBet: insertPrivateBet,
	InsertPublicBet: insertPublicBet
}