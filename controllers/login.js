// The utils is a function library for general purposes 
var Utils = require('../utils/utils.js');
// The user model (mongoose schema)
var User = require('../models/user');
// The login validator contains request validation functions
var Validator = require('../validators/login_validator.js');

// Use the paypal token to find the user 
// and return the user back to the client.
// this user will act as the context for every action.
function loginUser(req, res)
{
	if (!Validator.LoginUserValidation(req, res)) return;
	
	var where = {paypal_token : req.body.paypal_token};
	User.findOne(where, function(err, user)
	{
		if ((err) || (!user)) res.json(Utils.HandleError(err, "could not login user!"));
		else res.json(user);
	});	
}

// Insert a new user for sign up 
// and return the user model back to the client
function signUpUser(req, res)
{
	if (!Validator.SignUpUserValidation(req, res)) return;
	
	var user = new User();
	user.paypal_token = req.body.paypal_token;
	user.first_name = req.body.first_name;
	user.last_name = req.body.last_name;
	user.username = req.body.username;
	user.email = req.body.email;
	promise = user.save();
	promise.then(function(doc)
	{
		res.json(user);
	},
	function(err)
	{
		res.json(Utils.HandleError(err, "could not sign up user!")); 
	});
}
	
module.exports = {
	LoginUser: loginUser,
	SignUpUser: signUpUser 
}