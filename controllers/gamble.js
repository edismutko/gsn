// The utils is a function library for general purposes 
var Utils = require('../utils/utils.js');
// The gamble model (mongoose schema)
var Gamble = require('../models/gamble');
// The bet model (mongoose schema)
var Bet = require('../models/bet');
// The account_activity model (mongoose schema)
var AccountActivity = require('../models/account_activity');
// The user model (mongoose schema)
var User = require('../models/user');
// The gamble validator contains request validation functions
var Validator = require('../validators/gamble_validator.js');

// Insert a new gamble and also create a new withdrawal account activiy
// And update the user account balance by subtructing the bet amount
function insertGamble(req, res, user_id, bet_id)
{
	if (!Validator.InsertGambleValidation(req, res, user_id, bet_id)) return;
	Gamble.findOne({user_id : user_id, bet_id : bet_id}, 
	function (err, old_gamble) 
	{
		if (err)
		{
			res.json(Utils.HandleError(err,"Could not insert new gamble!"));
		}
		else if (old_gamble)
		{
			res.json(Utils.HandleError(err,"Gamble already exists!"));
		}
		else
		{
			Bet.findById(bet_id,
			function (err, bet)
			{
				if ((err) || (!bet)) res.json(Utils.HandleError(err,"Could not insert new gamble!"));
				else
				{
					var gamble = new Gamble();
					gamble.user_id = user_id;
					gamble.username = req.body.username;
					gamble.bet_id = bet_id;
					gamble.bet_title = bet.title;
					gamble.bet_option = req.body.bet_option;
					gamble.bet_start_date = bet.start_date;
					gamble.bet_end_date = bet.end_date;
					var promise = gamble.save();
					promise.then(function(doc)
					{
						withdrawFromAccountBalance(req, res, bet, gamble);
					},
					function(err)
					{
						res.json(Utils.HandleError(err,"Could not insert new gamble!"));
					});
				}
			});
		}
	});

}

// Returns the gamble found upon the given user_id & bet_id
function getGamble(req, res, user_id, bet_id)
{
	Gamble.findOne({user_id : user_id, bet_id : bet_id}, 
		function (err, gamble) 
		{
			if ((err) || (!gamble)) res.json(Utils.handleError(err, "Could not find gamble!"));
			else res.json(gamble);
		});
}

// Update the gamble's bet_option
// This is the only field available for update 
// because the rest field comes from the bet
function updateGamble(req, res, user_id, bet_id, gamble_id)
{
	if (!Validator.UpdateGambleValidation(req, res)) return;
	Gamble.findOne({_id:gamble_id, user_id:user_id, bet_id:bet_id}, 
	function (err, gamble) 
	{
		if ((err) || (!gamble)) res.json(Utils.HandleError(err, "Could not find gamble!"))
		else
		{
			gamble.bet_option = req.body.bet_option;
			gamble.save(function(updatedDoc) 
			{
				res.json(gamble);
			},
			function(err)
			{
				res.json(Utils.HandleError(err, "Could not update gamble!"));
			});
		}
	});
}

// Delete gamble and also create a new deposit account activiy
// And update the user account balance by adding the bet amount
function deleteGamble(req, res, user_id, bet_id, gamble_id)
{
	Gamble.findOne({_id:gamble_id, user_id:user_id, bet_id:bet_id},
	function(err, gamble)
	{
		if ((err) || (!gamble)) res.json(Utils.HandleError(err, "Could not find gamble!"));
		else
		{
			if (!(((new Date(gamble.bet_start_date)) > (new Date())) && ((new Date(gamble.bet_end_date)) > (new Date()))))
			{
				res.json(Utils.HandleError(err, "The gamble is active and could not be deleted!"));
			}
			else
			{
				Gamble.remove({_id:gamble._id},
				function(err)
				{
					if (err) res.json(Utils.HandleError(err, "Could not remove gamble!"));
					else
					{
						depositToAccountBalance(req, res, user_id, bet_id);
					}
				});
			}
		}
	});
}

// Helper function for creating a new withdrawal account activiy
// And update the user account balance by adding the bet amount
function withdrawFromAccountBalance(req, res, bet, gamble)
{
	User.findById(gamble.user_id,
	function(err, user)
	{
		if ((err) || (!user)) res.json(Utils.HandleError(err, "Could not find account!"));
		else
		{
			user.account_balance = user.account_balance - bet.amount;
			var promise = user.save();
			promise.then(function()
			{
				
				var activity = new AccountActivity();
				activity.user_id = user._id;
				activity.username = user.username;
				activity.bet_id = bet._id;
				activity.bet_title = bet.title;
				activity.amount = bet.amount;
				activity.description = "Withdraw amount " + activity.amount;
				var promise = activity.save();
				promise.then(function()
				{
					res.json(gamble);
				},
				function(err)
				{
					res.json(Utils.HandleError(err, "Could not insert account activity!"));
				});
			},
			function(err)
			{
				res.json(Utils.HandleError(err, "Could not update account balance!"));
			});
		}
	});
}

// Helper function for creating a new deposit account activiy
// And update the user account balance by adding the bet amount
function depositToAccountBalance(req, res, user_id, bet_id)
{
	User.findById(user_id,
	function(err, user)
	{
		if ((err) || (!user)) res.json(Utils.HandleError(err, "Could not find account!"));
		else
		{
			Bet.findById(bet_id, 
			function(err, bet)
			{
				if ((err) || (!bet)) res.json(Utils.HandleError(err, "Could not find bet!"));
				else
				{
					user.account_balance = user.account_balance + bet.amount;
					var promise = user.save();
					promise.then(function()
					{
						var activity = new AccountActivity();
						activity.user_id = user._id;
						activity.username = user.username;
						activity.bet_id = bet._id;
						activity.bet_title = bet.title;
						activity.amount = bet.amount;
						activity.description = "Deposit amount " + activity.amount;
						var promise = activity.save();
						promise.then(function()
						{
							res.json({result:"Gamble removed successfully"});
						},
						function(err)
						{
							res.json(Utils.HandleError(err, "Could not insert account activity!"));
						});
					},
					function(err)
					{
						res.json(Utils.HandleError(err, "Could not update account balance!"));
					});
				}
			});
		}
	});
}

module.exports = {
	InsertGamble:insertGamble,
	GetGamble:getGamble,
	UpdateGamble:updateGamble,
	DeleteGamble:deleteGamble
}