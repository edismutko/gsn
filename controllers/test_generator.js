var Utils = require('../utils/utils.js');
var User = require('../models/user');
var Bet = require('../models/bet');
var Gamble = require('../models/gamble');
var AccountActivity = require('../models/account_activity');

function randomNumber(max)
{
	return Math.floor(Math.random() * max);
}

function randomAmount()
{
    var amounts = [50,100,150,200,250,300];
    return amounts[randomNumber(6)];
}

function randomDate(start, end) {
    return new Date(start.getTime() + randomNumber(end.getTime() - start.getTime()));
}

function randomText()
{
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";
	var possibleA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	var size = randomNumber(10);
	if (size < 2) size = 2;
    for( var i=0; i < size; i++)
	{
		if (i==0) text = possible.charAt(randomNumber(possibleA.length));
        else text += possible.charAt(randomNumber(possible.length));
	}
    return text;
}

function generateUsers(req,res)
{
	for( var i=0; i < 20; i++) 
	{
		var user = new User();
		user.paypal_token = randomText();
		user.first_name = randomText();
		user.last_name = randomText();
		user.username = randomText().toLowerCase();
		user.email = user.username + "@mail.mta.ac.il";
		
		var promise = user.save();
		promise.then(function (doc) 
		{
			User.find({_id:{'$ne':doc._id}},function(err, docs)
				{
					var user1 = docs[randomNumber(docs.length)];
					var user2 = docs[randomNumber(docs.length)];
					var user3 = docs[randomNumber(docs.length)];
					var user4 = docs[randomNumber(docs.length)];
					var user5 = docs[randomNumber(docs.length)];
					var user6 = docs[randomNumber(docs.length)];
					admin = {user_id:doc._id,username:doc.username};
					generateOldBets(req,res,2,true,admin,
						[admin,
						 {user_id:user1._id,username:user1.username},
						 {user_id:user2._id,username:user2.username}]);
					generateActiveBets(req,res,4,true,admin,
						[admin,
					     {user_id:user3._id,username:user3.username},
						 {user_id:user4._id,username:user4.username}]);
					generateAvailableBets(req,res,6,true,admin,
						[admin,
						 {user_id:user5._id,username:user5.username},
						 {user_id:user6._id,username:user6.username}]);
				});
			res.json({done:"ok"});
		});
	}
}

function generateUserGambles(req, res)
{
	User.find({}, function(err, users)
	{
		Bet.find({}, function(err, bets)
		{
			var gambles = [];
			var activities = [];
			var usersToUpdate = [];
			for(i=0 ; i<200; i++)
			{
				var user = users[randomNumber(users.length)];
				var bet = bets[randomNumber(bets.length)];
				var gamble = new Gamble();
				gamble.user_id = user._id;
				gamble.username = user.username;
				gamble.bet_id = bet._id;
				gamble.bet_title = bet.title;
				gamble.bet_option = bet.options[randomNumber(4)];
				gamble.bet_start_date = bet.start_date;
				gamble.bet_end_date = bet.end_date;
				if (bet.end_date < new Date())
				{
					gamble.is_win = (bet.option == gamble.bet_option);
				}
				gambles.push(gamble);
				
				if (bet.is_private)
				{
					if (bet.users.find(function(vip){
							return vip.user_id == user._id;
						}))
					{
						var activity = new AccountActivity();
						activity.bet_id = bet._id;
						activity.bet_title = bet.title;
						activity.user_id = user._id;
						activity.username = user.username;
						activity.amount = bet.amount;
						activities.push(activity);
						if (gamble.is_win)
						{
							user.amount += activity.amount;
						}
						else if (gamble.is_win === false)
						{
							user.amount -= activity.amount;
						}
						usersToUpdate.push(user);
					}
				}
				else
				{
					var activity = new AccountActivity();
					activity.bet_id = bet._id;
					activity.bet_title = bet.title;
					activity.user_id = user._id;
					activity.username = user.username;
					activity.amount = bet.amount;
					activities.push(activity);
					if (gamble.is_win)
					{
						user.amount += activity.amount;
					}
					else if (gamble.is_win === false)
					{
						user.amount -= activity.amount;
					}
					usersToUpdate.push(user);
				}
			}
			gambles.forEach(function(item,index)
			{
				item.save();
			});
			activities.forEach(function(item,index)
			{
				item.save();
			});
			usersToUpdate.forEach(function(item,index)
			{
				item.save();
			});
		})
	});
	res.json({result:"ok"});
}

function generateOldBets(req,res, count, is_private, admin, users)
{
	var options = [{option : "1"},{option : "2"},{option : "3"},{option : "4"}];
	for( var i=0; i < count; i++) 
	{
		var bet = new Bet();
		bet.title = randomText();
		bet.description = randomText();
		bet.options = options;
		bet.result = options[randomNumber(4)];
		bet.amount = randomAmount();
		bet.start_date = randomDate(randomDate(new Date(2010, 1, 1), new Date(2010, 9, 9)),randomDate(new Date(2011, 1, 2), new Date(2011, 9, 9)));
		bet.end_date = randomDate(randomDate(new Date(2012, 1, 1), new Date(2014, 1, 1)),randomDate(new Date(2014, 1, 2), new Date(2016, 8, 8)));

		var promise = bet.save();
		promise.then(function (doc) {
			res.json({done:"ok"});
		});
	}
}

function generateActiveBets(req,res, count, is_private, admin, users)
{
	var options = [{option : "1"},{option : "2"},{option : "3"},{option : "4"}];
	for( var i=0; i < count; i++ )
	{
		var bet = new Bet();
		bet.title = randomText();
		bet.description = randomText();
		bet.options = options;
		bet.result = options[randomNumber(4)];
		bet.amount = randomAmount();
		bet.start_date = randomDate(new Date(2016, 0, 1), new Date(2016, 8, 8));
		bet.end_date = randomDate(new Date(), new Date(2017, 8, 8));
		if ((is_private) && (admin))
		{
			bet.is_private = true;
			bet.admin = admin;
			if (users)
			{
				bet.users = users;
			}
			else
			{
				bet.users = [];
			}
		}
		var promise = bet.save();
		promise.then(function (doc) {
			res.json({done:"ok"});
		});
	}
}

function generateAvailableBets(req,res, count, is_private, admin, users)
{
	var options = [{option : "1"},{option : "2"},{option : "3"},{option : "4"}];
	for( var i=0; i < count; i++) 
	{
		var bet = new Bet();
		bet.title = randomText();
		bet.description = randomText();
		bet.options = options;
		bet.result = options[randomNumber(4)];
		bet.amount = randomAmount();
		bet.start_date = randomDate(new Date(2017, 0, 1), new Date(2017, 8, 8));
		bet.end_date = randomDate(new Date(2017, 9, 9), new Date(2018, 8, 8));
		
		var promise = bet.save();
		promise.then(function (doc) {
			res.json({done:"ok"});
		});
	}
}
	
module.exports = {
	GenerateUsers: generateUsers,
	GenerateOldPublicBets: generateOldBets,
	GenerateAvailablePublicBets: generateAvailableBets,
	GenerateActivePublicBets: generateActiveBets,
	GenerateUserGambles: generateUserGambles
}