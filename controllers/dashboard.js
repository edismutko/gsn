// The utils is a function library for general purposes 
var Utils = require('../utils/utils.js');
// The bet model (mongoose schema)
var Bet = require('../models/bet'); 
// The gamble model (mongoose schema)
var Gamble = require('../models/gamble');
// The gamble controller
var GambleController = require('../controllers/gamble.js');

// Returns all the public bets available for gambling
// meanning the start date is bogger then now
function getAvilablePublicBets(req, res)
{
	//where start_date > now & is_private = false
	var where = {start_date : {$gt: new Date()}, is_private : false};
	Bet.find(where, 
	function(err, bets)
	{
		if (err) res.json(Utils.handleError(err, "Could not retrieve user available public bets!"));
		else res.json(bets);
	});
}

// Returns the all user's private bets
// bets where the user is eather an admin or a prticipant
function getUserPrivateBets(req, res, user_id)
{
	var where = { users : {$elemMatch: { user_id : user_id }}, is_private : true};
	Bet.find(where, 
	function(err, bets)
	{	
		if (err) res.json(Utils.handleError(err, "Could not retrieve user private bets!"));
		else res.json(bets);
	});
}

// Returns all active bets that the user has already gambled on
function getUserActiveBets(req, res, user_id)
{
	Gamble.find({user_id : user_id, start_date : {$lt: new Date()}, end_date : {$gt: new Date()}},
	function(err, gambles)
	{
		if (err) res.json(Utils.HandleError(err,"Could not find gambles!"));
		else
		{
			var bets_ids = [];
			for (var i = 0; i < gambles.length; i++) 
			{
				bets_ids.push(gambles[i].bet_id);
			}
			if (bets_ids.length > 0)
			{
				Bet.find({_id:{$in:bets_ids}},
				function(err, bets)
				{
					if (err) res.json(Utils.handleError(err, "Could not retrieve user active bets!"));
					else res.json(bets);
				});
			}
			else
			{
				res.json([]);
			}
		}
	});
}

// Returns all gambles that the user has won
function getUserGamblesWinnings(req, res, id)
{
	var where = {user_id : id, is_win : true};
	Gamble.find(where, function(err, gambles)
	{
		if (err) res.json(Utils.handleError(err, "Could not retrieve user public bets winnings!"));
		else res.json(gambles);
	});
}

// Returns all gambles that the user has lost
function getUserGamblesLosses(req, res, user_id)
{
	var where = {user_id : user_id, is_win : false, bet_end_date : {$lt: Date()}};
	Gamble.find(where, function(err, gambles)
	{
		if (err) res.json(Utils.handleError(err, "Could not retrieve user public bets losses!"));
		else res.json(gambles);
	});
}

// Returns all user's gambles
function getUserGambles(req, res, user_id)
{
	var where = {user_id : user_id};
	Gamble.find(where, function(err, gambles)
	{ 
		if (err) res.json(Utils.handleError(err, "Could not retrieve user gamble!"));
		else res.json(gambles);
	});
}

// Insert a new gamble 
function insertUserGamble(req, res, user_id, bet_id)
{
	GambleController.InsertGamble(req,res, user_id, bet_id);
}

module.exports = {
	GetAvilablePublicBets: getAvilablePublicBets,
	GetUserPrivateBets: getUserPrivateBets,
	GetUserActiveBets: getUserActiveBets,
	GetUserGamblesWinnings: getUserGamblesWinnings,
	GetUserGamblesLosses: getUserGamblesLosses,
	GetUserGambles: getUserGambles,
	InsertUserGamble: insertUserGamble
}