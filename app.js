// port for the mongodb server
const mongodb_port = 27017;
// port for the node js server
const listening_port = 3000;
// import express package
var express = require('express');
// define express app
var app = express();
// import mongoose package
var mongoose = require('mongoose');

// connect to mongodb
mongoose.connect('mongodb://localhost:' + mongodb_port + '/gsn',function(err,db){
    if(err) console.log(err);
});

// Get & setup router with the relevant routings
var router = express.Router();
require("./routings/login.js").SetupRouter(router);
require("./routings/account.js").SetupRouter(router);
require("./routings/dashboard.js").SetupRouter(router);
require("./routings/bet.js").SetupRouter(router);
require("./routings/gamble.js").SetupRouter(router);
require("./routings/test_generator.js").SetupRouter(router);

// Import body-parser for acquiring the post as json 
var bodyParser = require('body-parser');
// Define use of body-parser midleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Set the root for the gsn api
app.use('/gsn', router);

app.listen(listening_port, function () {
  console.log('GSN app listening on port ' + listening_port + '!');
});
