// Validate string type
function isString(str)
{
	return ((typeof(str) == "string") || (str instanceof String))
}
// Validate numeric type
function isNumeric(num) 
{
  return !isNaN(parseFloat(num)) && isFinite(num);
}
// Validate boolean type
function isBoolean(bool)
{
	return ((typeof(num) == "boolean") || (str instanceof Boolean));
}
// Validate date type
function isDate(dte)
{
	return (dte instanceof Date);
}
// Validate array type
function isArray(arr)
{
	return Array.isArray(arr);
}
// General error handling
function handleError(err, msg)
{
	if (err)
	{
		console.log(err);
		return {success : false, message : msg, error : err};
	}
	else 
	{
		return {success : false, message : msg};
	}
}

module.exports = {
	HandleError: handleError,
	IsString: isString,
	IsNumeric: isNumeric,
	IsBoolean: isBoolean,
	IsDate: isDate,
	IsArray: isArray
}